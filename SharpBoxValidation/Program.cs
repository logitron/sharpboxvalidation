﻿using AppLimit.CloudComputing.SharpBox;
using AppLimit.CloudComputing.SharpBox.StorageProvider.DropBox;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBoxSample
{
    class Program
    {
        static DropBoxConfiguration DB_CONFIG = DropBoxConfiguration.GetStandardConfiguration();
        const string REQUEST_TOKEN_KEY = "";
        const string REQUEST_TOKEN_SECRET = "";

        static void Main(string[] args)
        {
            var accessToken = GenerateAccessToken();
            var success = ValidateAccessToken(accessToken);

            if (success)
                Console.WriteLine("Successfully Retrieved Valid Access Token");
            else
                Console.WriteLine("There was an error generating a valid access token");

            Console.ReadKey();
        }

        static ICloudStorageAccessToken GenerateAccessToken()
        {
            var callback = DB_CONFIG.AuthorizationCallBack.ToString().Replace("sharpox", "sharpbox");
            DB_CONFIG.AuthorizationCallBack = new Uri(callback);

            var requestToken = DropBoxStorageProviderTools
                .GetDropBoxRequestToken(DB_CONFIG, REQUEST_TOKEN_KEY, REQUEST_TOKEN_SECRET);

            var authUrl = DropBoxStorageProviderTools
                .GetDropBoxAuthorizationUrl(DB_CONFIG, requestToken);

            AuthorizeRequest(authUrl);

            var accessToken = DropBoxStorageProviderTools
                .ExchangeDropBoxRequestTokenIntoAccessToken(
                    DB_CONFIG, 
                    REQUEST_TOKEN_KEY, 
                    REQUEST_TOKEN_SECRET, 
                    requestToken);

            return accessToken;
        }

        static bool ValidateAccessToken(ICloudStorageAccessToken accessToken)
        {
            var result = false;
            var cloudStorage = new CloudStorage();

            try
            {
                cloudStorage.Open(DB_CONFIG, accessToken);
                result = true;
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Generating New Access Token...");
                accessToken = GenerateAccessToken();

                result = ValidateAccessToken(accessToken);
            }

            return result;
        }

        static void AuthorizeRequest(string authUrl)
        {
            Process.Start(authUrl);

            Console.Write("Once you've completed authorization, press any key to continue...");
            Console.ReadKey();
            Console.WriteLine();
        }
    }
}
