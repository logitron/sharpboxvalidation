#SharpBoxSample

This project uses SharpBox to generate a valid access token

Before using this application for personal use, you will need to update the key/secret at the beginning of the Program.cs file:

* REQUEST _ TOKEN _ KEY
* REQUEST _ TOKEN _ SECRET